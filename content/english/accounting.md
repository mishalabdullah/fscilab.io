---
title: "Accounting"
date: 2023-01-19T18:00:34+05:30
author: false
draft: false
discussionlink: https://codema.in/d/VkCybRTY/fsci-services-accounting
---
## Index

- <a href="#balance-of-services-after-the-end of-2022">Balance of services after the end of 2022</a>

- <a href="#2022-income--expenses">2022 Income & Expenses</a>

- <a href="#expected-costs-for-the-year-2023">Expected Costs for the year 2023</a>

- <a href="#expected-costs-for-the-year-2024">Expected Costs for the year 2024</a>

## Balance of services after the end of 2022

<b>Note: All amounts are in INR (Indian rupee). A plus sign before the amount means excess funds than required to run the service. Minus sign means deficit.</b>

<b>Funding Campaign 2022</b>: +23050

<b>Poddery</b>: -51721.31

<b>Codema</b>: -9737.82

<b>Meet</b>: +19356.41

<b>Videos</b>: -4419.73

<b>Mailing Lists</b>: +4794.42

<b>Total</b>: -18678.03

## 2022 Income & Expenses

<b>Expense</b>: 56,976.95

<b>Income</b>: 48,944.06

<b>Deficit</b>: 8,032.89

## Expected Costs for the year 2023

<b>Poddery</b>: 37,000

<b>Codema + Meet + Wiki</b>: 8,000

<b>Videos</b>: 9,000

<b>Lists</b>: 6,000

<b>Total</b>: 60,000

## Expected Costs for the year 2024

<b>Poddery</b>: 40,000

<b>Codema + Meet + Wiki</b>: 9,000

<b>Videos</b>: 11,000

<b>Lists</b>: 7,000

<b>Total</b>: 67,000

For the consolidated income expenditure report and our funding campaign discussions, please visit our <b>[Codema thread](https://codema.in/d/VkCybRTY/fsci-services-accounting)</b>.
