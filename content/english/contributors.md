---
title: "Contributors"
---

<style>
table, td, th {  
  border: 1px solid #ddd;
  text-align: left;
}

table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  padding: 15px;
}
</style>

Accounting details are on [this page](/accounting).

## 2023-24 Crowd Funding Campaign

**Campaign target: 1,50,000INR**
<br>
<table>
  <tr>
    <th>Name of the donor</th>
    <th>Amount donated(INR)</th>
    <th>Service for which donation was made</th>
  </tr>
  <tr>
    <td>FOSS United</td>
    <td>1,00,000</td>
    <td>FSCI General fund</td>
  </tr>
  <tr>
    <td>Snehal Shekatkar</td>
    <td>2000</td>
    <td>FSCI General fund</td>
  </tr>
  <tr>
    <td>Ravi Dwivedi</td>
    <td>1000</td>
    <td>FSCI General fund</td>
  </tr>
  <tr>
    <td>Snehal Shekatkar</td>
    <td>1600</td>
    <td>FSCI General fund</td>
  </tr>
</table>
<br>
<br>

## 2022 Crowd Funding Campaign 

List of all the contributors to FSCI's 2022 Funding Campaign (in INR).

**Amount Received from 2022 campaign** - 28,730 INR <br>

<table>
  <tr>
    <th>Name of the donor</th>
    <th>Amount donated(INR)</th>
    <th>Service for which donation was made</th>
  </tr>
  <tr>
    <td>Ravi Dwivedi</td>
    <td>1000</td>
    <td>XMPP poddery</td>
  </tr>
  <tr>
    <td>Deepak Gothwal</td>
    <td>500</td>
    <td>poddery.com</td>
  </tr>
  <tr>
    <td>Sahil Dhiman</td>
    <td>2000</td>
    <td>FSCI General fund</td>
  </tr>
  <tr>
    <td>Abhilash V</td>
    <td>600</td>
    <td>FSCI General fund</td>
  </tr>
  <tr>
    <td>Nilesh</td>
    <td>1000</td>
    <td>FSCI General fund</td>
  </tr>
  <tr>
    <td>GN</td>
    <td>5000</td>
    <td>FSCI General fund</td>
  </tr>
  <tr>
    <td>Dhanesh</td>
    <td>500</td>
    <td>FSCI General fund</td>
  </tr>
  <tr>
    <td>Ravi</td>
    <td>500</td>
    <td>matrix poddery</td>
  <tr>
    <td>Jyothis Jagan</td>
    <td>3000</td>
    <td>FSCI General fund</td>
  </tr>
  <tr>
    <td>Subin</td>
    <td>5000</td>
    <td>matrix poddery</td>
  </tr>
  <tr>
    <td>Kannan</td>
    <td>2680</td>
    <td>FSCI General fund</td>
  </tr>
  <tr>
    <td>Snehal Shekatkar</td>
    <td>2000</td>
    <td>FSCI General fund</td>
  </tr>
  <tr>
    <td>Mostly Harmless</td>
    <td>4200</td>
    <td>FSCI General fund</td>
  </tr>
  <tr>
    <td>Dhruva Punde</td>
    <td>750</td>
    <td>Loomio-codema.in</td>
  </tr>
  </tr>
    <td></td>
    <td><b>Total Amount = 28,730</b></td>
    <td></td>
  </tr>
</table>


### We are very grateful to our contributors for supporting the ideals behind running this service.
<br>
<br>
<br>
<br>
<br>
<br>
<br>
