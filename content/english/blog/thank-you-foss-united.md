---
title: "Thank You Foss United"
date: 2023-07-18T04:27:18+05:30
draft: false
tags: ["funding", "foss-united"]
---
We are pleased to announce that we got a grant from [FOSS United](https://fossunited.org/grants) worth ₹1 lakh, which helps us in maintaining the existing various services we provide which includes, but not limited to poddery.com, codema.in etc. Our funds are managed by non-profit [Navodaya Networks](https://navodaya.network/) as we are not a registered organization.

We thank FOSS United and Navodaya for their support. Thanks to contributions like these, due to which we are able to run privacy respecting services for the general public. Consider [donating](https://fsci.in/donate) to FSCI today to keep our services running. You can see how we spend this money at the [accounting page](https://fsci.in/accounting/).

For two years 2023 and 2024, we targeted ₹1,50,000 as amount for crowdfunding campaign. Getting ₹1,00,000 from this grant means our target from donors is reduced to ₹50,000 for these two years.

We thank all the people who donate to us and volunteer time to run these services. Without them it would not have been possible. We are always looking for new people to volunteer and if you are interested, [talk to us](https://fsci.in/contact). [Here](https://fsci.in/#poddery) is list of services we provide. Feel free to use them!

Help us through your donations so that community backed privacy respecting services continue running.
